module scaleway-dedibox-api

go 1.18

require (
	github.com/sirupsen/logrus v1.8.1
	golang.org/x/oauth2 v0.0.0-20220524215830-622c5d57e401
	github.com/golang/protobuf v1.4.2 // indirect
	golang.org/x/net v0.0.0-20220127200216-cd36cc0744dd // indirect
	golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e // indirect
	google.golang.org/appengine v1.6.6 // indirect
	google.golang.org/protobuf v1.25.0 // indirect
)
