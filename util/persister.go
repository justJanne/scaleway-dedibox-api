package util

type Persister[T any] interface {
	Restore() (*T, error)
	Persist(data *T) error
}
