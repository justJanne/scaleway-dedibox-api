package util

import (
	"encoding/json"
	"fmt"
	"os"
)

type FilePersister[T any] struct {
	path string
}

func NewFilePersister[T any](path string) Persister[T] {
	return Persister[T](FilePersister[T]{path})
}

func (persister FilePersister[T]) Restore() (*T, error) {
	file, err := os.OpenFile(persister.path, os.O_RDONLY, 0600)
	if err != nil {
		return nil, fmt.Errorf("filepersister: could not restore file %s\n  %w", persister.path,
			fmt.Errorf("could not open file\n  %w", err))
	}
	var data T
	err = json.NewDecoder(file).Decode(&data)
	if err != nil {
		return nil, fmt.Errorf("filepersister: could not restore file %s\n  %w", persister.path,
			fmt.Errorf("could not deserialize data\n  %w", err))
	}
	return &data, nil
}

func (persister FilePersister[T]) Persist(data *T) error {
	file, err := os.OpenFile(persister.path, os.O_RDWR|os.O_CREATE, 0600)
	if err != nil {
		return fmt.Errorf("filepersister: could not persist file %s\n  %w", persister.path,
			fmt.Errorf("could not open file\n  %w", err))
	}
	err = json.NewEncoder(file).Encode(data)
	if err != nil {
		return fmt.Errorf("filepersister: could not persist file %s\n  %w", persister.path,
			fmt.Errorf("could not serialize data\n  %w", err))
	}
	return nil
}
