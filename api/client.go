package api

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"scaleway-dedibox-api/httputil"
)

type Client struct {
	http    *httputil.Client
	baseUrl *url.URL
}

func NewClient(client *httputil.Client, baseUrl *url.URL) Client {
	if client == nil {
		client = httputil.NewClient(nil, nil)
	}
	return Client{
		http:    client,
		baseUrl: baseUrl,
	}
}

type parsedError struct {
	Code  int    `json:"code"`
	Error string `json:"error"`
}

func parseError(body io.ReadCloser) (parsedError, error) {
	var result parsedError
	return result, json.NewDecoder(body).Decode(&result)
}

func (c Client) doRequest(request *http.Request, target any) error {
	response, err := c.http.Do(request)
	if err != nil {
		return fmt.Errorf("api: could not execute request\n  %w", err)
	}
	if !httputil.IsHttpStatusSuccess(response.StatusCode) {
		parsedError, err := parseError(response.Body)
		if err != nil {
			return fmt.Errorf("api: got non-success status: %s", response.Status)
		} else {
			return fmt.Errorf("api: got non-success status: %s, code: %d, reason: %s",
				response.Status, parsedError.Code, parsedError.Error)
		}
	}
	if target != nil {
		if err := json.NewDecoder(response.Body).Decode(&target); err != nil {
			return fmt.Errorf("api: could not deserialize response body\n  %w", err)
		}
	}
	return nil
}
