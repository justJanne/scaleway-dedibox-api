package api

import (
	"fmt"
	"path"
	"strconv"
)

type RaidControllerId int
type RaidControllerRef string

const raidControllerPath = "/api/v1/server/hardware/raidController/"

func ToRaidControllerRef(id RaidControllerId) RaidControllerRef {
	return RaidControllerRef(path.Join(raidControllerPath, strconv.Itoa(int(id))))
}

func ToRaidControllerId(ref RaidControllerRef) (RaidControllerId, error) {
	prefix, id := path.Split(string(ref))
	if prefix != diskPath {
		return 0, fmt.Errorf("api.model_raidcontroller: invalid raid controller ref %s", ref)
	}
	parsed, err := strconv.Atoi(id)
	if err != nil {
		return 0, fmt.Errorf("api.model_raidcontroller: invalid raid controller ref %s\n  %w", ref, err)
	}
	return RaidControllerId(parsed), nil
}

type RaidLevel string

const (
	RaidLevelNone RaidLevel = "NORAID"
	RaidLevel0    RaidLevel = "RAID0"
	RaidLevel1    RaidLevel = "RAID1"
	RaidLevel5    RaidLevel = "RAID5"
	RaidLevel6    RaidLevel = "RAID6"
)

type SupportedRaidLevel struct {
	RaidLevel RaidLevel `json:"raid_level,omitempty"`
	Min       int       `json:"min,omitempty"`
	Max       int       `json:"max,omitempty"`
}

type RaidController struct {
	Id                  RaidControllerId     `json:"id,omitempty"`
	Model               string               `json:"model,omitempty"`
	Disks               []Disk               `json:"disks,omitempty"`
	SupportedRaidLevels []SupportedRaidLevel `json:"supported_raid_levels,omitempty"`
}
