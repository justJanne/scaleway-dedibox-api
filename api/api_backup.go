package api

import (
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

func (c Client) GetBackup(id ServerId) (FTPBackup, error) {
	var result FTPBackup

	path, err := FTPBackupPath(id)
	if err != nil {
		return result, fmt.Errorf("api.backup: could not parse url\n  %w", err)
	}
	request, err := http.NewRequest("GET", c.baseUrl.ResolveReference(path).String(), nil)
	if err != nil {
		return result, fmt.Errorf("api.backup: could not build request\n  %w", err)
	}
	err = c.doRequest(request, &result)
	if err != nil {
		return result, fmt.Errorf("api.backup: could not get backup\n  %w", err)
	}
	return result, nil
}

type EditBackupParams struct {
	Password   string
	AutoLogin  bool
	AclEnabled bool
}

func (c Client) EditBackup(id ServerId, params EditBackupParams) error {
	body := strings.NewReader(url.Values{
		"password":    []string{params.Password},
		"autologin":   []string{strconv.FormatBool(params.AutoLogin)},
		"acl_enabled": []string{strconv.FormatBool(params.AclEnabled)},
	}.Encode())

	path, err := FTPBackupPath(id)
	if err != nil {
		return fmt.Errorf("api.backup: could not parse url\n  %w", err)
	}
	request, err := http.NewRequest("PUT", c.baseUrl.ResolveReference(path).String(), body)
	if err != nil {
		return fmt.Errorf("api.backup: could not build request\n  %w", err)
	}
	err = c.doRequest(request, nil)
	if err != nil {
		return fmt.Errorf("api.backup: could not edit backup\n  %w", err)
	}
	return nil
}
