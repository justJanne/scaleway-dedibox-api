package api

import (
	"fmt"
	"path"
	"strconv"
)

type DiskId int
type DiskRef string

const diskPath = "/api/v1/server/hardware/disk/"

func ToDiskRef(id DiskId) DiskRef {
	return DiskRef(path.Join(diskPath, strconv.Itoa(int(id))))
}

func ToDiskId(ref DiskRef) (DiskId, error) {
	prefix, id := path.Split(string(ref))
	if prefix != diskPath {
		return 0, fmt.Errorf("api.model_disk: invalid disk ref %s", ref)
	}
	parsed, err := strconv.Atoi(id)
	if err != nil {
		return 0, fmt.Errorf("api.model_disk: invalid disk ref %s\n  %w", ref, err)
	}
	return DiskId(parsed), nil
}

type DiskType = string

const (
	DiskTypeSATA DiskType = "SATA"
	DiskTypeSAS  DiskType = "SAS"
	DiskTypeSSHD DiskType = "SSHD"
	DiskTypeNVME DiskType = "NVME"
)

type Disk struct {
	Id             DiskId                        `json:"id,omitempty"`
	Connector      string                        `json:"connector,omitempty"`
	Type           DiskType                      `json:"type,omitempty"`
	Capacity       int                           `json:"capacity,omitempty"`
	RaidController WrappedRef[RaidControllerRef] `json:"raid_controller,omitempty"`
}
