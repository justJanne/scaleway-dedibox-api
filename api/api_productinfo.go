package api

import (
	"fmt"
	"net/http"
)

func (c Client) GetProductInfo(id ServerId) (ProductInfo, error) {
	var result ProductInfo

	path, err := ProductInfoPath(id)
	if err != nil {
		return result, fmt.Errorf("api.productinfo: could not parse url\n  %w", err)
	}
	request, err := http.NewRequest("GET", c.baseUrl.ResolveReference(path).String(), nil)
	if err != nil {
		return result, fmt.Errorf("api.productinfo: could not build request\n  %w", err)
	}
	err = c.doRequest(request, &result)
	if err != nil {
		return result, fmt.Errorf("api.productinfo: could not get productinfo\n  %w", err)
	}
	return result, nil
}
