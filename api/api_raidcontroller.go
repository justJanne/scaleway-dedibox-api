package api

import (
	"fmt"
	"net/http"
	"net/url"
)

func (c Client) GetRaidController(ref RaidControllerRef) (RaidController, error) {
	var result RaidController

	path, err := url.Parse(string(ref))
	if err != nil {
		return result, fmt.Errorf("api.raidcontroller: could not parse url\n  %w", err)
	}
	request, err := http.NewRequest("GET", c.baseUrl.ResolveReference(path).String(), nil)
	if err != nil {
		return result, fmt.Errorf("api.raidcontroller: could not build request\n  %w", err)
	}
	err = c.doRequest(request, &result)
	if err != nil {
		return result, fmt.Errorf("api.raidcontroller: could not get raidcontroller\n  %w", err)
	}
	return result, nil
}
