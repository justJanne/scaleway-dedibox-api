package api

import (
	"fmt"
	"net/http"
	"net/url"
	"strings"
)

func (c Client) GetBMCSession(id ServerId) (BMCSession, error) {
	var result BMCSession

	path, err := BMCSessionPath(id)
	if err != nil {
		return result, fmt.Errorf("api.bmcsession: could not parse url\n  %w", err)
	}
	request, err := http.NewRequest("GET", c.baseUrl.ResolveReference(path).String(), nil)
	if err != nil {
		return result, fmt.Errorf("api.bmcsession: could not build request\n  %w", err)
	}
	err = c.doRequest(request, &result)
	if err != nil {
		return result, fmt.Errorf("api.bmcsession: could not get bmc session\n  %w", err)
	}
	return result, nil
}

func (c Client) CreateBMCSession(id ServerId, authorizedIp string) error {
	body := strings.NewReader(url.Values{
		"ip": []string{authorizedIp},
	}.Encode())

	path, err := BMCSessionPath(id)
	if err != nil {
		return fmt.Errorf("api.bmcsession: could not parse url\n  %w", err)
	}
	request, err := http.NewRequest("POST", c.baseUrl.ResolveReference(path).String(), body)
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	if err != nil {
		return fmt.Errorf("api.bmcsession: could not build request\n  %w", err)
	}
	err = c.doRequest(request, nil)
	if err != nil {
		return fmt.Errorf("api.bmcsession: could not create bmc session\n  %w", err)
	}
	return nil
}

func (c Client) CloseBMCSession(id ServerId) error {
	path, err := BMCSessionPath(id)
	if err != nil {
		return fmt.Errorf("api.bmcsession: could not parse url\n  %w", err)
	}
	request, err := http.NewRequest("DELETE", c.baseUrl.ResolveReference(path).String(), nil)
	if err != nil {
		return fmt.Errorf("api.bmcsession: could not build request\n  %w", err)
	}
	err = c.doRequest(request, nil)
	if err != nil {
		return fmt.Errorf("api.bmcsession: could not close bmc session\n  %w", err)
	}
	return nil
}
