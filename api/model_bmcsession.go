package api

import (
	"net/url"
	"path"
	"strconv"
)

const bmcSessionPath = "/api/v1/server/bmc/session/"

func BMCSessionPath(id ServerId) (*url.URL, error) {
	return url.Parse(path.Join(bmcSessionPath, strconv.Itoa(int(id))))
}

type BMCSession struct {
	Url        string   `json:"url,omitempty"`
	Login      string   `json:"login,omitempty"`
	Password   string   `json:"password,omitempty"`
	Expiration *TimeDto `json:"expiration,omitempty"`
}
