package api

import (
	"net/url"
	"path"
	"strconv"
)

const rescueImagePath = "/api/v1/server/rescue_images/"

type RescueImageId string

func RescueImagePath(id ServerId) (*url.URL, error) {
	return url.Parse(path.Join(rescueImagePath, strconv.Itoa(int(id))))
}
