package api

import (
	"net/url"
	"path"
	"strconv"
	"time"
)

type OperatingSystemId = int

const operatingSystemPath = "/api/v1/server/operatingSystems/"

func OperatingSystemPath(id ServerId) (*url.URL, error) {
	return url.Parse(path.Join(operatingSystemPath, strconv.Itoa(int(id))))
}

type OperatingSystemType = string

const (
	OperatingSystemTypeServer         OperatingSystemType = "server"
	OperatingSystemTypePanel          OperatingSystemType = "panel"
	OperatingSystemTypeDesktop        OperatingSystemType = "desktop"
	OperatingSystemTypeLive           OperatingSystemType = "live"
	OperatingSystemTypeCustom         OperatingSystemType = "custom"
	OperatingSystemTypeVirtualization OperatingSystemType = "virtualization"
)

type OperatingSystemArch = string

const (
	OperatingSystemArch32bit OperatingSystemArch = "32 bits"
	OperatingSystemArch64bit OperatingSystemArch = "64 bits"
)

type OperatingSystem struct {
	Id        OperatingSystemId   `json:"id,omitempty"`
	Name      string              `json:"name,omitempty"`
	Version   string              `json:"version,omitempty"`
	Type      OperatingSystemType `json:"type,omitempty"`
	Arch      OperatingSystemArch `json:"arch,omitempty"`
	Release   *time.Time          `json:"release,omitempty"`
	EndOfLife *time.Time          `json:"end_of_life,omitempty"`
}
