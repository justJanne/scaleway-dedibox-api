package api

import (
	"fmt"
	"time"
)

type TimeDto time.Time

func (dto TimeDto) MarshalJSON() ([]byte, error) {
	if y := time.Time(dto).Year(); y < 0 || y >= 10000 {
		// RFC 3339 is clear that years are 4 digits exactly.
		// See golang.org/issue/4556#c15 for more discussion.
		return nil, fmt.Errorf("api.time: year outside of range [0,9999]")
	}

	b := make([]byte, 0, len(time.RFC3339Nano)+2)
	b = append(b, '"')
	b = time.Time(dto).AppendFormat(b, time.RFC3339Nano)
	b = append(b, '"')
	return b, nil
}

func (dto *TimeDto) UnmarshalJSON(data []byte) error {
	// Ignore null, like in the main JSON package.
	if string(data) == "null" {
		return nil
	}
	// Fractional seconds are handled implicitly by Parse.
	var err error
	result, err := time.Parse(`"`+time.RFC1123+`"`, string(data))
	if err != nil {
		return err
	}
	*dto = TimeDto(result)
	return nil
}
