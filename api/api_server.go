package api

import (
	"fmt"
	"net/http"
	"net/url"
	"strings"
)

func (c Client) GetServers() ([]ServerRef, error) {
	var result []ServerRef

	path, err := url.Parse(serverPath)
	if err != nil {
		return result, fmt.Errorf("api.server: could not parse url\n  %w", err)
	}
	request, err := http.NewRequest("GET", c.baseUrl.ResolveReference(path).String(), nil)
	if err != nil {
		return result, fmt.Errorf("api.server: could not build request\n  %w", err)
	}
	err = c.doRequest(request, &result)
	if err != nil {
		return result, fmt.Errorf("api.server: could not list servers\n  %w", err)
	}
	return result, nil
}

func (c Client) GetServer(ref ServerRef) (Server, error) {
	var result Server

	path, err := url.Parse(string(ref))
	if err != nil {
		return result, fmt.Errorf("api.server: could not parse url\n  %w", err)
	}
	request, err := http.NewRequest("GET", c.baseUrl.ResolveReference(path).String(), nil)
	if err != nil {
		return result, fmt.Errorf("api.server: could not build request\n  %w", err)
	}
	err = c.doRequest(request, &result)
	if err != nil {
		return result, fmt.Errorf("api.server: could not get server\n  %w", err)
	}
	return result, nil
}

func (c Client) UpdateServer(ref ServerRef, hostname string) error {
	body := strings.NewReader(url.Values{
		"hostname": []string{hostname},
	}.Encode())

	path, err := url.Parse(string(ref))
	if err != nil {
		return fmt.Errorf("api.server: could not parse url\n  %w", err)
	}
	request, err := http.NewRequest("PUT", c.baseUrl.ResolveReference(path).String(), body)
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	if err != nil {
		return fmt.Errorf("api.server: could not build request\n  %w", err)
	}
	err = c.doRequest(request, nil)
	if err != nil {
		return fmt.Errorf("api.server: could not update server\n  %w", err)
	}
	return nil
}

func (c Client) BootServerNormal(id ServerId) error {
	path, err := ServerBootPath(id, BootModeNormal)
	if err != nil {
		return fmt.Errorf("api.server: could not parse url\n  %w", err)
	}
	request, err := http.NewRequest("POST", c.baseUrl.ResolveReference(path).String(), nil)
	if err != nil {
		return fmt.Errorf("api.server: could not build request\n  %w", err)
	}
	err = c.doRequest(request, nil)
	if err != nil {
		return fmt.Errorf("api.server: could not boot server in normal mode\n  %w", err)
	}
	return nil
}

func (c Client) BootServerRescue(id ServerId, image RescueImageId) (RescueCredentials, error) {
	var result RescueCredentials
	body := strings.NewReader(url.Values{
		"image": []string{string(image)},
	}.Encode())

	path, err := ServerBootPath(id, BootModeRescue)
	if err != nil {
		return result, fmt.Errorf("api.server: could not parse url\n  %w", err)
	}
	request, err := http.NewRequest("POST", c.baseUrl.ResolveReference(path).String(), body)
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	if err != nil {
		return result, fmt.Errorf("api.server: could not build request\n  %w", err)
	}
	err = c.doRequest(request, nil)
	if err != nil {
		return result, fmt.Errorf("api.server: could not boot server in rescue mode\n  %w", err)
	}
	return result, nil
}

type ServerNotificationParams struct {
	Reason string
	Email  string
}

func (c Client) ShutdownServer(id ServerId, params ServerNotificationParams) error {
	body := strings.NewReader(url.Values{
		"reason": []string{params.Reason},
		"email":  []string{params.Email},
	}.Encode())

	path, err := ServerShutdownPath(id)
	if err != nil {
		return fmt.Errorf("api.server: could not parse url\n  %w", err)
	}
	request, err := http.NewRequest("POST", c.baseUrl.ResolveReference(path).String(), body)
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	if err != nil {
		return fmt.Errorf("api.server: could not build request\n  %w", err)
	}
	err = c.doRequest(request, nil)
	if err != nil {
		return fmt.Errorf("api.server: could not shutdown server\n  %w", err)
	}
	return nil
}

func (c Client) RebootServer(id ServerId, params ServerNotificationParams) error {
	body := strings.NewReader(url.Values{
		"reason": []string{params.Reason},
		"email":  []string{params.Email},
	}.Encode())

	path, err := ServerRebootPath(id)
	if err != nil {
		return fmt.Errorf("api.server: could not parse url\n  %w", err)
	}
	request, err := http.NewRequest("POST", c.baseUrl.ResolveReference(path).String(), body)
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	if err != nil {
		return fmt.Errorf("api.server: could not build request\n  %w", err)
	}
	err = c.doRequest(request, nil)
	if err != nil {
		return fmt.Errorf("api.server: could not reboot server\n  %w", err)
	}
	return nil
}
