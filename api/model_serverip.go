package api

import (
	"net"
	"path"
)

const serverIPPath = "/api/v1/server/ip/"

type ServerIPRef string

func ToServerIPRef(ip net.IP) ServerIPRef {
	return ServerIPRef(path.Join(serverIPPath, ip.String()))
}

type ServerIPType string

const (
	ServerIPTypePublic   ServerIPType = "public"
	ServerIPTypePrivate  ServerIPType = "private"
	ServerIPTypeFailover ServerIPType = "failover"
)

type ServerIPStatus string

const (
	ServerIPStatusActive   ServerIPStatus = "active"
	ServerIPStatusUpdating ServerIPStatus = "updating"
)

type ServerIP struct {
	Address         net.IP                `json:"address,omitempty"`
	Type            ServerIPType          `json:"type,omitempty"`
	Reverse         string                `json:"reverse,omitempty"`
	MacAddress      string                `json:"mac,omitempty"`
	Destination     net.IP                `json:"destination,omitempty"`
	Status          ServerIPStatus        `json:"status,omitempty"`
	SwitchPortState string                `json:"switch_port_state,omitempty"`
	Server          WrappedRef[ServerRef] `json:"server,omitempty"`
}
