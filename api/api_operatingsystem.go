package api

import (
	"fmt"
	"net/http"
)

func (c Client) GetOperatingSystems(id ServerId) ([]OperatingSystem, error) {
	var result []OperatingSystem

	path, err := OperatingSystemPath(id)
	if err != nil {
		return result, fmt.Errorf("api.operatingsystem: could not parse url\n  %w", err)
	}
	request, err := http.NewRequest("GET", c.baseUrl.ResolveReference(path).String(), nil)
	if err != nil {
		return result, fmt.Errorf("api.operatingsystem: could not build request\n  %w", err)
	}
	err = c.doRequest(request, &result)
	if err != nil {
		return result, fmt.Errorf("api.operatingsystem: could not list operatingsystems\n  %w", err)
	}
	return result, nil
}
