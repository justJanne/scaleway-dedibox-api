package api

type WrappedRef[T any] struct {
	Ref T `json:"$ref,omitempty"`
}
