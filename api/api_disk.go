package api

import (
	"fmt"
	"net/http"
	"net/url"
)

func (c Client) GetDisk(ref DiskRef) (Disk, error) {
	var result Disk

	path, err := url.Parse(string(ref))
	if err != nil {
		return result, fmt.Errorf("api.disk: could not parse url\n  %w", err)
	}
	request, err := http.NewRequest("GET", c.baseUrl.ResolveReference(path).String(), nil)
	if err != nil {
		return result, fmt.Errorf("api.disk: could not build request\n  %w", err)
	}
	err = c.doRequest(request, &result)
	if err != nil {
		return result, fmt.Errorf("api.disk: could not get disk\n  %w", err)
	}
	return result, nil
}
