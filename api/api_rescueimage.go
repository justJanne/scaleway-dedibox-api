package api

import (
	"fmt"
	"net/http"
)

func (c Client) GetRescueImages(id ServerId) ([]RescueImageId, error) {
	var result []RescueImageId

	path, err := RescueImagePath(id)
	if err != nil {
		return result, fmt.Errorf("api.rescueimage: could not parse url\n  %w", err)
	}
	request, err := http.NewRequest("GET", c.baseUrl.ResolveReference(path).String(), nil)
	if err != nil {
		return result, fmt.Errorf("api.rescueimage: could not build request\n  %w", err)
	}
	err = c.doRequest(request, &result)
	if err != nil {
		return result, fmt.Errorf("api.rescueimage: could not list rescueimages\n  %w", err)
	}
	return result, nil
}
