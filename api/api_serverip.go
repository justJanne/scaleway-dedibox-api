package api

import (
	"fmt"
	"net/http"
	"net/url"
)

func (c Client) GetIPAddress(ref ServerIPRef) (ServerIP, error) {
	var result ServerIP

	path, err := url.Parse(string(ref))
	if err != nil {
		return result, fmt.Errorf("api.serverip: could not parse url\n  %w", err)
	}
	request, err := http.NewRequest("GET", c.baseUrl.ResolveReference(path).String(), nil)
	if err != nil {
		return result, fmt.Errorf("api.serverip: could not build request\n  %w", err)
	}
	err = c.doRequest(request, &result)
	if err != nil {
		return result, fmt.Errorf("api.serverip: could not get serverip\n  %w", err)
	}
	return result, nil
}
