package api

import (
	"net/url"
	"path"
	"strconv"
)

const ftpBackupPath = "/api/v1/server/backup/"

func FTPBackupPath(id ServerId) (*url.URL, error) {
	return url.Parse(path.Join(ftpBackupPath, strconv.Itoa(int(id))))
}

type FTPBackup struct {
	Login          string `json:"login,omitempty"`
	Server         string `json:"server,omitempty"`
	Active         bool   `json:"active,omitempty"`
	AclEnabled     bool   `json:"acl_enabled,omitempty"`
	AutoLogin      bool   `json:"auto_login,omitempty"`
	QuotaSpace     int    `json:"quota_space,omitempty"`
	QuotaSpaceUsed int    `json:"quota_space_used,omitempty"`
	QuotaFiles     int    `json:"quota_files,omitempty"`
	QuotaFilesUsed int    `json:"quota_files_used,omitempty"`
}
