package api

import (
	"net/url"
	"path"
	"strconv"
)

const productInfoPath = "/api/v1/server/product/"

func ProductInfoPath(id ServerId) (*url.URL, error) {
	return url.Parse(path.Join(productInfoPath, strconv.Itoa(int(id))))
}

type ProductInfo struct {
	ProductId int    `json:"product_id"`
	Name      string `json:"name,omitempty"`
	Price     int    `json:"price,omitempty"`
}
