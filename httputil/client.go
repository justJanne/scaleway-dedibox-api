package httputil

import (
	"context"
	"fmt"
	"golang.org/x/net/context/ctxhttp"
	"net/http"
)

type Client struct {
	client             http.Client
	ctx                context.Context
	requestMiddleware  *RequestMiddleware
	responseMiddleware *ResponseMiddleware
}

func NewClient(ctx context.Context, client *http.Client) *Client {
	if client == nil {
		client = &http.Client{}
	}
	return &Client{
		ctx:    ctx,
		client: *client,
	}
}

func (c *Client) WithRequestMiddleware(middleware RequestMiddleware) *Client {
	if c.requestMiddleware != nil {
		middleware = RequestMiddlewares(*c.requestMiddleware, middleware)
	}
	return &Client{
		client:             c.client,
		requestMiddleware:  &middleware,
		responseMiddleware: c.responseMiddleware,
	}
}

func (c *Client) WithResponseMiddleware(middleware ResponseMiddleware) *Client {
	if c.responseMiddleware != nil {
		middleware = ResponseMiddlewares(*c.responseMiddleware, middleware)
	}
	return &Client{
		client:             c.client,
		requestMiddleware:  c.requestMiddleware,
		responseMiddleware: &middleware,
	}
}

func (c *Client) Do(req *http.Request) (*http.Response, error) {
	if c.requestMiddleware != nil {
		if err := (*c.requestMiddleware)(req); err != nil {
			return nil, fmt.Errorf("httputil.client: could not execute request middleware\n  %w", err)
		}
	}
	var resp *http.Response
	var err error
	if c.ctx == nil {
		resp, err = c.client.Do(req)
	} else {
		resp, err = ctxhttp.Do(c.ctx, &c.client, req)
	}
	if err != nil {
		return nil, fmt.Errorf("httputil.client: could not execute request\n  %w", err)
	}
	if resp == nil {
		return nil, fmt.Errorf("httputil.client: got a nil response during an http request")
	}
	if c.responseMiddleware != nil {
		if err := (*c.responseMiddleware)(req, resp); err != nil {
			return nil, fmt.Errorf("httputil.client: could not execute response middleware\n  %w", err)
		}
	}

	return resp, nil
}

func IsHttpStatusSuccess(status int) bool {
	return status >= 200 && status < 300
}
