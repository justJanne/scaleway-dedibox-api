package httputil

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"net/http"
	"net/http/httputil"
	"strings"
)

func TracingMiddleware(traceAll bool) ResponseMiddleware {
	return func(req *http.Request, resp *http.Response) error {
		if traceAll || !IsHttpStatusSuccess(resp.StatusCode) {
			originalAuth := req.Header.Get("Authorization")
			if originalAuth != "" {
				if strings.HasPrefix(originalAuth, "Bearer ") {
					req.Header.Set("Authorization", "Bearer [redacted]")
				} else {
					req.Header.Set("Authorization", "[redacted]")
				}
			}
			reqTrace, err := httputil.DumpRequestOut(req, false)
			if err != nil {
				return fmt.Errorf("httputil.middleware_tracing: could not trace http request %w", err)
			}
			// only dump body for errors
			respTrace, err := httputil.DumpResponse(resp, traceAll || !IsHttpStatusSuccess(resp.StatusCode))
			if err != nil {
				return fmt.Errorf("httputil.middleware_tracing: could not trace http response %w", err)
			}
			logrus.Infoln(
				strings.Join(
					[]string{
						"--- HTTP TRACE ---",
						string(reqTrace),
						strings.TrimSpace(string(respTrace)),
					},
					"\n",
				),
			)
		}
		return nil
	}
}
