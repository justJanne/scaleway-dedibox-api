package httputil

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"golang.org/x/oauth2"
	"net/http"
	"time"
)

var tokenSourceMiddlewareLog = logrus.WithField("file", "httputil.middleware_tokensource")

func TokenSourceMiddleware(tokenSource oauth2.TokenSource) RequestMiddleware {
	return func(req *http.Request) error {
		token, err := tokenSource.Token()
		if err != nil {
			return err
		}
		if token.Type() != "Bearer" {
			return fmt.Errorf("httputil.middleware_tokensource: cannot use token of type %s for authorization", token.Type())
		}
		tokenSourceMiddlewareLog.Debugln("authorizing with token having expiry of", token.Expiry.Format(time.RFC3339))
		req.Header.Set(
			"Authorization",
			fmt.Sprintf("Bearer %s", token.AccessToken),
		)
		return nil
	}
}
