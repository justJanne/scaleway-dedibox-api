package httputil

import (
	"net/http"
)

type RequestMiddleware func(req *http.Request) error
type ResponseMiddleware func(req *http.Request, resp *http.Response) error

func RequestMiddlewares(middlewares ...RequestMiddleware) RequestMiddleware {
	return func(req *http.Request) error {
		for _, middleware := range middlewares {
			if err := middleware(req); err != nil {
				return err
			}
		}
		return nil
	}
}

func ResponseMiddlewares(middlewares ...ResponseMiddleware) ResponseMiddleware {
	return func(req *http.Request, resp *http.Response) error {
		for _, middleware := range middlewares {
			if err := middleware(req, resp); err != nil {
				return err
			}
		}
		return nil
	}
}
