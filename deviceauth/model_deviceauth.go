package deviceauth

import "time"

type DeviceAuth struct {
	DeviceCode              string        `json:"device-code"`
	UserCode                string        `json:"user-code"`
	VerificationURI         string        `json:"verification-uri"`
	VerificationURIComplete string        `json:"verification-uri-complete"`
	Expires                 time.Time     `json:"expires"`
	Interval                time.Duration `json:"interval"`
}
