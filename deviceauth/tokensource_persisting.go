package deviceauth

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"golang.org/x/oauth2"
	"scaleway-dedibox-api/util"
	"sync"
)

var persistingTokenSourceLog = logrus.WithField("file", "deviceauth.tokensource_persisting")

// reuseTokenSource is a TokenSource that holds a single token in storage
// and validates its expiry before each call to retrieve it with
// Token. If it's expired, it will be auto-refreshed using the
// fallback TokenSource.
type persistingTokenSource struct {
	fallback  oauth2.TokenSource // called when token is expired.
	mutex     sync.Mutex         // guards token
	token     *oauth2.Token
	persister util.Persister[oauth2.Token]
}

// Token returns the current token if it's still valid, else will
// refresh the current token and return the new one.
func (source *persistingTokenSource) Token() (*oauth2.Token, error) {
	source.mutex.Lock()
	defer source.mutex.Unlock()
	if source.token.Valid() {
		return source.token, nil
	}
	token, err := source.persister.Restore()
	if err != nil {
		persistingTokenSourceLog.Debugln("requesting new token")
		token, err = source.fallback.Token()
	} else {
		persistingTokenSourceLog.Debugln("token restored")
	}
	if err != nil {
		return nil, fmt.Errorf("deviceauth.tokensource_persisting: could not request new token\n  %w", err)
	}
	source.token = token
	persistingTokenSourceLog.Debugln("persisting token")
	err = source.persister.Persist(token)
	if err != nil {
		return nil, fmt.Errorf("deviceauth.tokensource_persisting: could not persist new token\n  %w", err)
	}
	return token, nil
}

func PersistingTokenSource(fallback oauth2.TokenSource, persister util.Persister[oauth2.Token]) (oauth2.TokenSource, error) {
	// Don't wrap a reuseTokenSource in itself. That would work,
	// but cause an unnecessary number of mutex operations.
	// Just build the equivalent one.
	if wrapped, ok := fallback.(*persistingTokenSource); ok {
		fallback = wrapped.fallback
	}
	return &persistingTokenSource{
		fallback:  fallback,
		persister: persister,
	}, nil
}
