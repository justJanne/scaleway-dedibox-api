package deviceauth

import (
	"fmt"
	"golang.org/x/oauth2"
)

type deviceAuthTokenSource struct {
	deviceAuthConfig *DeviceAuthConfig
}

func (s *deviceAuthTokenSource) Token() (*oauth2.Token, error) {
	device, err := s.deviceAuthConfig.AuthDevice(AccessTypeOffline)
	if err != nil {
		return nil, fmt.Errorf("deviceauth.tokensource_deviceauth: could not authenticate device\n  %w", err)
	}
	fmt.Printf(
		"\n  Please go to the following URL and enter this code '%s' to verify this oauth app:\n\n    %s\n\n\n",
		device.UserCode,
		device.VerificationURI,
	)
	token, err := s.deviceAuthConfig.Poll(device, AccessTypeOffline)
	if err != nil {
		return nil, fmt.Errorf("deviceauth.tokensource_deviceauth: did not receive a device token\n  %w", err)
	}
	return token, nil
}

func DeviceAuthTokenSource(config *DeviceAuthConfig) oauth2.TokenSource {
	return &deviceAuthTokenSource{
		deviceAuthConfig: config,
	}
}
