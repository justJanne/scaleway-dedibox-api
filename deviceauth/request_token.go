package deviceauth

import (
	"encoding/json"
	"fmt"
	"golang.org/x/oauth2"
	"io"
	"io/ioutil"
	"mime"
	"net/http"
	"net/url"
	"scaleway-dedibox-api/httputil"
	"strconv"
	"strings"
	"time"
)

func cloneURLValues(v url.Values) url.Values {
	v2 := make(url.Values, len(v))
	for k, vv := range v {
		v2[k] = append([]string(nil), vv...)
	}
	return v2
}

// newTokenRequest returns a fallback *http.Request to retrieve a fallback token
// from tokenURL using the provided clientID, clientSecret, and POST
// body parameters.
func newTokenRequest(tokenURL, clientID, clientSecret string, v url.Values, authStyle AuthStyle) (*http.Request, error) {
	if authStyle == AuthStyleInParams {
		v = cloneURLValues(v)
		if clientID != "" {
			v.Set("client_id", clientID)
		}
		if clientSecret != "" {
			v.Set("client_secret", clientSecret)
		}
	}
	req, err := http.NewRequest("POST", tokenURL, strings.NewReader(v.Encode()))
	if err != nil {
		return nil, fmt.Errorf("deviceauth.request_token: could not build token request\n  %w", err)
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	if authStyle == AuthStyleInHeader {
		req.SetBasicAuth(url.QueryEscape(clientID), url.QueryEscape(clientSecret))
	}
	return req, nil
}

// tokenJSON is the struct representing the HTTP response from OAuth2
// providers returning a token in JSON form.
type tokenJSON struct {
	AccessToken  string      `json:"access_token"`
	TokenType    string      `json:"token_type"`
	RefreshToken string      `json:"refresh_token"`
	ExpiresIn    durationDto `json:"expires_in"` // at least PayPal returns string, while most return number
}

func (config *DeviceAuthConfig) doTokenRoundTrip(request *http.Request) (*oauth2.Token, error) {
	now := time.Now()
	response, err := config.Client.Do(request)
	if err != nil {
		return nil, fmt.Errorf("deviceauth.request_token: unable to fetch token\n  %w", err)
	}
	body := io.LimitReader(response.Body, maximumBodySize)
	if !httputil.IsHttpStatusSuccess(response.StatusCode) {
		fullBody, err := ioutil.ReadAll(body)
		if err != nil {
			return nil, fmt.Errorf("deviceauth.request_token: unable to fetch token\n  %w", err)
		}
		return nil, &oauth2.RetrieveError{
			Response: response,
			Body:     fullBody,
		}
	}

	var token *oauth2.Token
	content, _, _ := mime.ParseMediaType(response.Header.Get("Content-Type"))
	switch content {
	case "application/x-www-form-urlencoded", "text/plain":
		fullBody, err := ioutil.ReadAll(body)
		if err != nil {
			return nil, fmt.Errorf("deviceauth.request_token: could not read token response\n  %w", err)
		}
		vals, err := url.ParseQuery(string(fullBody))
		if err != nil {
			return nil, fmt.Errorf("deviceauth.request_token: could not parse token response\n  %w", err)
		}
		token = &oauth2.Token{
			AccessToken:  vals.Get("access_token"),
			TokenType:    vals.Get("token_type"),
			RefreshToken: vals.Get("refresh_token"),
		}
		e := vals.Get("expires_in")
		expires, _ := strconv.Atoi(e)
		if expires != 0 {
			token.Expiry = now.Add(time.Duration(expires) * time.Second)
		}
	default:
		var tj tokenJSON
		if err = json.NewDecoder(body).Decode(&tj); err != nil {
			return nil, fmt.Errorf("deviceauth.request_token: could not deserialize token response\n  %w", err)
		}
		token = &oauth2.Token{
			AccessToken:  tj.AccessToken,
			TokenType:    tj.TokenType,
			RefreshToken: tj.RefreshToken,
			Expiry:       tj.ExpiresIn.expiry(now),
		}
	}
	if token.AccessToken == "" {
		return nil, fmt.Errorf("deviceauth.request_token: server response missing access_token")
	}
	return token, nil
}

func (config *DeviceAuthConfig) retrieveToken(params url.Values, authStyle AuthStyle) (*oauth2.Token, error) {
	req, err := newTokenRequest(config.Endpoint.TokenURL, config.ClientID, config.ClientSecret, params, authStyle)
	if err != nil {
		return nil, fmt.Errorf("deviceauth.request_token: could not retrieve token\n  %w", err)
	}
	token, err := config.doTokenRoundTrip(req)
	// Don’t overwrite `RefreshToken` with an empty value if this was a token refreshing request.
	if token != nil && token.RefreshToken == "" {
		token.RefreshToken = params.Get("refresh_token")
	}
	if err != nil {
		return nil, fmt.Errorf("deviceauth.request_token: could not retrieve token\n  %w", err)
	}
	return token, nil
}
