package deviceauth

// AuthStyle is a copy of the golang.org/x/oauth2 package's AuthStyle type.
type AuthStyle int

const (
	AuthStyleInParams AuthStyle = 1
	AuthStyleInHeader AuthStyle = 2
)
