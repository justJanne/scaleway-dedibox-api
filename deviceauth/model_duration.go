package deviceauth

import (
	"encoding/json"
	"math"
	"time"
)

type durationDto int32

func (d durationDto) expiry(now time.Time) (t time.Time) {
	if d != 0 {
		return now.Add(time.Duration(d) * time.Second)
	}
	return
}

func (d *durationDto) UnmarshalJSON(b []byte) error {
	if len(b) == 0 || string(b) == "null" {
		return nil
	}
	var n json.Number
	err := json.Unmarshal(b, &n)
	if err != nil {
		return err
	}
	i, err := n.Int64()
	if err != nil {
		return err
	}
	if i > math.MaxInt32 {
		i = math.MaxInt32
	}
	*d = durationDto(i)
	return nil
}
