package deviceauth

import (
	"encoding/json"
	"fmt"
	"golang.org/x/oauth2"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"scaleway-dedibox-api/httputil"
	"strings"
)

type jsonDeviceAuth struct {
	DeviceCode              string      `json:"device_code"`
	UserCode                string      `json:"user_code"`
	VerificationURI         string      `json:"verification_uri"`
	VerificationURL         string      `json:"verification_url"`
	VerificationURIComplete string      `json:"verification_uri_complete,omitempty"`
	ExpiresIn               durationDto `json:"expires_in"`
	Interval                durationDto `json:"interval,omitempty"`
}

func (config *DeviceAuthConfig) retrieveDeviceAuth(params url.Values) (*jsonDeviceAuth, error) {
	request, err := http.NewRequest("POST", config.Endpoint.DeviceCodeURL, strings.NewReader(params.Encode()))
	if err != nil {
		return nil, fmt.Errorf("deviceauth.request_deviceauth: unable to build device auth request\n  %w", err)
	}
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	reponse, err := config.Client.Do(request)
	if err != nil {
		return nil, fmt.Errorf("deviceauth.request_deviceauth: unable to authenticate device\n  %w", err)
	}

	body := io.LimitReader(reponse.Body, maximumBodySize)
	if !httputil.IsHttpStatusSuccess(reponse.StatusCode) {
		fullBody, err := ioutil.ReadAll(body)
		if err != nil {
			return nil, fmt.Errorf("deviceauth.request_deviceauth: unable to authenticate device\n  %w", err)
		}
		return nil, &oauth2.RetrieveError{
			Response: reponse,
			Body:     fullBody,
		}
	}

	var deviceAuth = &jsonDeviceAuth{}
	err = json.NewDecoder(body).Decode(&deviceAuth)
	if err != nil {
		return nil, fmt.Errorf("deviceauth.request_deviceauth: unable to deserialize device auth response\n  %w", err)
	}

	return deviceAuth, nil
}

func parseError(err error) string {
	e, ok := err.(*oauth2.RetrieveError)
	if ok {
		eResp := make(map[string]string)
		_ = json.Unmarshal(e.Body, &eResp)
		return eResp["error"]
	}
	return ""
}
